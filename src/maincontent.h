/*
 * Kararot - easy configuration and troubleshooting with one click
 * Copyright (C) 2019  Guo Yunhe <i@guoyunhe.me>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONTENT_H
#define CONTENT_H

#include "ui_maincontent.h"

// Qt5
#include <QScopedPointer>
#include <QWidget>

/**
 * @todo write docs
 */
class MainContent : public QWidget
{
    Q_OBJECT

public:
    explicit MainContent(QWidget *parent = nullptr);

private:
    QScopedPointer<Ui::MainContent> ui;
};

#endif // CONTENT_H
