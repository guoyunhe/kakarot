#include <cstdlib>

#include <QApplication>
#include <QCommandLineParser>

#include <KAboutData>
#include <KLocalizedString>

#include "mainwindow.h"

int main (int argc, char *argv[])
{
    QApplication app(argc, argv);
    KLocalizedString::setApplicationDomain("kakarot");

    KAboutData aboutData(
        // The program name used internally. (componentName)
        QStringLiteral("kakarot"),
                         // A displayable program name string. (displayName)
                         i18n("Kakarot"),
                         // The program version string. (version)
                         QStringLiteral("1.0"),
                         // Short description of what the app does. (shortDescription)
                         i18n("Easy configuration and troubleshooting with one click"),
                         // The license this code is released under
                         KAboutLicense::GPL_V3,
                         // Copyright Statement (copyrightStatement = QString())
                         i18n("(c) 2018"),
                         // Optional text shown in the About box.
                         // Can contain any information desired. (otherText)
                         i18n("Have a lot of fun..."),
                         // The program homepage string. (homePageAddress = QString())
                         QStringLiteral("https://gitlab.com/guoyunhe/kakarot"),
                         // The bug report email address
                         // (bugsEmailAddress = QLatin1String("submit@bugs.kde.org")
                         QStringLiteral("i@guoyunhe.me"));
    aboutData.addAuthor(i18n("Guo Yunhe"), i18n("Author"), QStringLiteral("i@guoyunhe.me"),
                        QStringLiteral("https://guoyunhe.me/"), QStringLiteral("guoyunhe"));
    KAboutData::setApplicationData(aboutData);

    QCommandLineParser parser;
    aboutData.setupCommandLine(&parser);
    parser.process(app);
    aboutData.processCommandLine(&parser);

    MainWindow* window = new MainWindow();
    window->show();

    return app.exec();
}
