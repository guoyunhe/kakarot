#include "mainwindow.h"
#include "maincontent.h"

MainWindow::MainWindow(QWidget *parent) : KXmlGuiWindow(parent)
{
    MainContent *content = new MainContent(this);
    setCentralWidget(content);
    setupGUI();
}
