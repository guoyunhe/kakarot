# Contributing

This project just started. All ideas, suggestions and contribution are welcome.

## Design decisions

1. Only for vector icon design. Not for general graphical design. Not for bitmap
   icon design.
2. Support SVG format with PNG exporting feature.
3. Based on Qt 5 (Qt Widgets) and KDE Frameworks 5. Not QtQuick (QML), which is
   too resource consuming.
4. Grids, colors, templates for Material design (Android), Breeze (KDE), etc.
