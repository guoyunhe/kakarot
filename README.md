# Kakarot

Kakarot is the assistant that aims to fix system configuration, crashing and
failures with just one click.

## Feature

- General
  - Pulseaudio
    - Restart
  - Trash
    - Repair trash bin and empty trash
- openSUSE
  - Install codecs from Packman
- KDE
  - Baloo
    - Repair index database and stop crashing
